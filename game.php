<?php

class Game
{
    private $boxes;

    private $prisoners;

    private $maxIteration = null;

    public $status;

    public $log;

    public function __construct()
    {
        $this->boxes = range(0, 89);
        shuffle($this->boxes);

        $this->prisoners = range(0, 89);
    }

    public function start($verbose = true)
    {
        foreach ($this->prisoners as $prisoner) {

            $this->log .=  "Prisoner #{$prisoner} : ";

            $attempts = $this->find($prisoner);
            $this->maxIteration = ($this->maxIteration > $attempts) ? $this->maxIteration : $attempts;

            $this->log .=  " ({$attempts}  attempts)" . PHP_EOL;
        }

        $this->status = ($this->maxIteration <= 45) ? 'Win' : 'Fail';
        $this->log .= $this->status . PHP_EOL;

        if ($verbose) {
            echo  $this->log;
        }
    }

    private function find($number, $attempt = 1, $box = null)
    {
        if ($box === null) $box = $number;

        $this->log .= "->open box {$box}";

        if ($number == $this->boxes[$box]) {
            return $attempt;
        }

        return $this->find($number, ++$attempt, $this->boxes[$box]);
    }
}

class Test
{
    private $iterations;

    private $passed = null;

    public function __construct($iterations)
    {
        $this->iterations = $iterations;
    }

    public function run()
    {
        for ($i = 1; $i <= $this->iterations; $i++) {
            $game = new Game;
            $game->start(false);

            if ($game->status === 'Win') {
                $this->passed++;
            }

            unset($game);
        }

        echo 'Effectiveness=', $this->passed*100/$this->iterations, '%', PHP_EOL;
    }
}

/**
 * Single game
 */
//(new Game)->start();

/**
 * Complex test
 */
(new Test(10000))->run();

